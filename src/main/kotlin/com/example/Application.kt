package com.example

import com.example.model.Comentari
import com.example.model.Pellicula
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.example.plugins.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

var pelis = mutableSetOf<Pellicula>()
var comentaris = mutableSetOf<Comentari>()
fun main() {
    // var pe = Pellicula("1", "aa", "2023", "bb", "c")

    // pelis.add(pe)
    // println(Json.encodeToString(pe))

    embeddedServer(Netty, port = 8080, host = "0.0.0.0", module = Application::module)
            .start(wait = true)
}

fun Application.module() {
    configureSerialization()
    configureRouting()
}
