package com.example.model

import kotlinx.serialization.Serializable

@Serializable
enum class Genere(var correct : String? = null) {
    Accio("Acció"),
    Aventures,
    CienciaFiccio("Ciència Ficció"),
    Comedia("Comèdia"),
    NoFiccio("No- Ficció"),
    Drama,
    Fantasia,
    Musical,
    Suspens,
    Terror;

    open fun string(): String{
        return correct ?: this.name

    }
}