package com.example.model

import kotlinx.serialization.Serializable


@Serializable
data class Comentari(
    var id: String, var idDeLaPellicula: String, var comentari: String, var dataDeCreacio: Long= time()
) {
    companion object {
        fun time() = System.currentTimeMillis()
    }
}
