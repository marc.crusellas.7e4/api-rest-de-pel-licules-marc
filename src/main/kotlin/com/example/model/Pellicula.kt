package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class Pellicula(
    var id: String,
    var titol: String,
    var any: String,
    var genere: String,
    var director: String) {


}
