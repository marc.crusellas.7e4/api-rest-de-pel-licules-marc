import com.example.comentaris
import com.example.model.Comentari
import com.example.model.Pellicula
import com.example.pelis
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.customerRouting() {

    get("all") {
        if (pelis.isNotEmpty()) {
            call.respond(pelis)
        } else {
            call.respondText("No Pel·lícules found", status = HttpStatusCode.OK)
        }
    }

    post("add") {

        try {
            val peli = call.receive<Pellicula>()

            if (pelis.map { it.id }.contains(peli.id)){
                 call.respondText("A customer with id ${peli.id}", status = HttpStatusCode.NotFound)
                return@post
            }
            pelis.add(peli)
            call.respondText("Pel·lícula stored correctly", status = HttpStatusCode.Created)

        } catch (aaaaaaaaaaa: Exception) {

            call.respondText("Pel·lícula not stored correctly", status = HttpStatusCode.Created)

        }

    }

    route("{id?}") {
        get {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id", status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            if (id != null) {
                for (customer in pelis) {
                    if (customer.id == id) return@get call.respond(customer)
                }
                call.respondText("No customer with id $id", status = HttpStatusCode.NotFound)
            } else {
                return@get call.respondText("ID NOT FOUND, or is not Int", status = HttpStatusCode.NotAcceptable)
            }
        }

        post("add") {

            val id = call.parameters["id"]
            if (id != null) {
                val comentari = call.receive<Comentari>()

                if (pelis.map { it.id }.contains(id)) {

                    val newID = (comentaris.mapNotNull { it.id.toIntOrNull() }.maxOfOrNull { it } ?: 0) + 1
                    val comentariFinal = Comentari(
                        newID.toString(),
                        id,
                        comentari.comentari,
                        comentari.dataDeCreacio
                    )
                    comentaris.add(comentariFinal)
                    return@post call.respondText(
                        "Comentari afegit!", status = HttpStatusCode.OK
                    )
                } else {
                    return@post call.respondText(
                        "Missing id", status = HttpStatusCode.NotFound
                    )
                }


            } else return@post call.respondText(
                "Missing id", status = HttpStatusCode.BadRequest
            )


        }

        get("comments") {
            val id = call.parameters["id"]
            if (id != null) {
                val coments = comentaris.filter { it.idDeLaPellicula == id }

                if (coments.isEmpty()) return@get call.respondText(
                        "No hi ha cometaris per a aquesta pel·lícula.", status = HttpStatusCode.BadRequest)

                call.respond(coments)
            } else call.respondText(
                    "Id no valida", status = HttpStatusCode.BadRequest)

        }

    }



    route("update") {
        post("{id?}") {
            val id = call.parameters["id"]

            if (id != null) {
                println("fase 2")
                val peli = call.receive<Pellicula>()
                if (pelis.map { it.id }.contains(id)) {
                    pelis.removeIf { it.id ==  id}
                    peli.id = id
                    pelis.add(peli)

                    call.respondText(
                        "Pel·lícula actualitzada.", status = HttpStatusCode.OK)

                } else call.respondText(
                    "La Pel·lícula no existeix.", status = HttpStatusCode.NotAcceptable)

            } else call.respondText(
                "La'id és incorrecte.", status = HttpStatusCode.NotModified)

        }
    }
    route("delete") {

        delete("{id?}") {
            val id = call.parameters["id"]
            if (id != null) {
                if (pelis.map { it.id }.contains(id)) {
                    pelis = pelis.filterNot { it.id == id }.toMutableSet()
                    comentaris.removeIf { it.idDeLaPellicula == id }
                     call.respondText(
                        "Pel·lícula eliminada.", status = HttpStatusCode.BadRequest
                    )
                } else call.respondText(
                        "Id no valida", status = HttpStatusCode.BadRequest
                    )

            } else call.respondText(
                    "Id no valida", status = HttpStatusCode.BadRequest)

        }
    }


}
